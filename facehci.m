clc
close all
clear all

gazedata = csvread('gazedata.csv',2,0);
sessions = mixed_csv('sessions.csv',',');
sessions(:,[1 2 4 5 6 7 9 10 11]) = cellfun(@(s) {str2double(s)},sessions(:,[1 2 4 5 6 7 9 10 11]));

for i=2:1:size(sessions)
    figure(i);
   
    A = sessions(i,3);
    imgA=imread( A{1} );
    B = sessions(i,8);
    imgB=imread( B{1} );
 
    Ap = uint8 (zeros(800, 600, 3));
    Ap = uint8( Ap);
    Bp = uint8 (zeros(800, 600, 3));
    Bp = uint8( Ap);
    %c=zeros(1050,1680,3);
    x1=1;
    for x=1:2:600
        y1=1;
        for y=1:2:800
            Ap(y,x,:)=imgA(y1,x1,:);
            Ap(y+1,x+1,:)=imgA(y1,x1,:);
            Ap(y,x+1,:)=imgA(y1,x1,:);
            Ap(y+1,x,:)=imgA(y1,x1,:);
            %Ap(y,x,)=imgA(y1,x1,2);
            %Ap(y,x,3)=imgA(y1,x1,3);
            y1=y1+1;
        end
        x1=x1+1;
    end
        x1=1;
    for x=1:2:600
        y1=1;
        for y=1:2:800
            Bp(y,x,:)=imgB(y1,x1,:);
            Bp(y+1,x+1,:)=imgB(y1,x1,:);
            Bp(y,x+1,:)=imgB(y1,x1,:);
            Bp(y+1,x,:)=imgB(y1,x1,:);
            %Ap(y,x,)=imgA(y1,x1,2);
            %Ap(y,x,3)=imgA(y1,x1,3);
            y1=y1+1;
        end
        x1=x1+1;
    end
    c=[zeros(62,1680,3);zeros(800,190,3) Ap zeros(800,100,3) Bp zeros(800,190,3);zeros(62,1680,3)];    
    image(c);
    for n = 2:1:size(gazedata)
        if(i==gazedata(n,1))
            hold on;
            plot(gazedata(n,3),gazedata(n,4),'r*');
            %c(uint8(gazedata(n,3))+1,uint8(gazedata(n,4))+1,:)=255;
            %c(uint8(gazedata(n,3))+1,uint8(gazedata(n,4))+2,:)=255;
            %c(uint8(gazedata(n,3))+2,uint8(gazedata(n,4))+2,:)=255;
            %c(uint8(gazedata(n,3))+2,uint8(gazedata(n,4))+1,:)=255;
        end
    end
    asd = sessions(i,2);
    if  asd{1} == 1
        plot(190:1:790,830,'gX');
    else
        plot(890:1:1490,830,'gX');
    end
    axis([0 1680 0 924]);
    %image(c);
end